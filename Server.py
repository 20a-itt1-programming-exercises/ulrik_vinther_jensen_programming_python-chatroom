import threading
import socket

#locahost and port number. Port should be above any reserved ports to get rid of issues
host = '127.0.0.1'
port = 46255

#intialize connection
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((host, port))
server.listen()

#intialize varibles for client
clients = []
nicknames = []

def broadcast(message):
    for client in clients:
        client.send(message)

def handle(client):
    while True:
        try:
            message = client.recv(1024)
            broadcast(message)
        except:
            index = clients.index(client)
            clients.remove(client)
            client.close()
            nickname = nicknames[index]
            broadcast(f'{nickname} left the chat!'.encode('UTF-8'))
            print(f'Client with nickname "{nickname}" has disconnected!')
            nicknames.remove(nickname)
            break


def recieve():
    while True:
        client, address = server.accept()
        print(f"Connected with {str(address)}")

        client.send("NICK".encode('UTF-8'))
        nickname = client.recv(1024).decode('UTF-8')
        nicknames.append(nickname)
        clients.append(client)

        print(f'Nickname of client joined is "{nickname}"!')
        broadcast(f'{nickname} joined the chat!'.encode('UTF-8'))
        client.send('You are now connected to the chatroom'.encode('UTF-8'))

        thread = threading.Thread(target=handle, args=(client,))
        thread.start()
print("Server is now listening...")
recieve()
